import 'package:map_operators/map_operators.dart';

void main() {
  var m = {'myKey': 'myVal'};

  // Alternative for m.kontainsKey('myKey'):
  print(m&'myKey'); // prints true.

  // Alternative for !m.containsKey('myKey'):
  print(m^'notExistsKey'); // prints true

  // Alternative for key access operator []:
  print(m/'myKey'); // prints myVal

  // You can't do this, because `[]=` operator is not overrided:
  // m/'newkey'='newVal'; // Syntax error

}
