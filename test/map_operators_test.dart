import 'package:test/test.dart';
import 'package:map_operators/map_operators.dart';

void main() {
  group('A group of tests', () {

    test('Contains key operator', () {
      expect( {2:3}&2, isTrue );
    });
    test('Not contains key operator', () {
      expect( {2:3}^2, isFalse );
    });

    test('Key access operator', (){
      expect({2:3} / 2, equals(3));
    });

  });
}
