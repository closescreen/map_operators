# More comfortable syntax for Map type

## Usage

A simple usage example:

```dart
import 'package:map_operators/map_operators.dart';

void main() {
  var m = {'myKey': 'myVal'};

  // Alternative for m['myKey']!=null:
  print(m&'myKey'); // prints true.

  // Alternative for m['myKey']==null:
  print(m^'notExistsKey'); // prints true

  // Alternative for key access operator []:
  print(m/'myKey'); // prints myVal
}
```
