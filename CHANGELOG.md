# 1.0.0

- Initial version, created by Stagehand

## 1.1.0

- `&` operator now checks value for not null
- `^` operator now checks value for null

## 1.1.1

-fix readme

## 2.0.0

- Migrated to null-safety
