extension MapOperators<T1, T2> on Map<T1, T2> {
  /// Alternative for this[key] != null
  bool operator &(key) => this[key] != null;

  /// Alternative for this[key] == null
  bool operator ^(key) => this[key] == null;

  /// Alternative for []
  T2? operator /(T1 key) => this[key];
}
